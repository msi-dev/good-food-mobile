import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import tw from "tailwind-react-native-classnames";
import IonIcons from "react-native-vector-icons/Ionicons";
import { BoltLightText, BoltSemiBoldText } from "../CustomText";
import { URL_BACK_END } from '../../Environment/Environment'

export default function RestaurantCard({ data, navigation }) {
	const { name, price, url } = data;
	return (
		<View style={tw`flex-col mr-4 justify-between w-full`}>
			<View style={tw`flex-row items-center mt-5 h-40 w-full relative`}>
				<TouchableOpacity
					style={tw`w-full h-full`}
					onPress={""}
				>
					<Image
						source={{uri: `${URL_BACK_END}${url}`}}
						style={tw`w-full h-full rounded-lg`}
					/>
				</TouchableOpacity>
			</View>
			<View style={tw`flex-col justify-between mt-1.5`}>
				<View style={tw`flex-row justify-between`}>
					<View style={tw`flex-col justify-between`}>
						<View style={tw`flex-row`}>
							<BoltSemiBoldText style={tw`text-lg`}>
								{name.length > 40
									? name.substring(0, 40 - 3) + "..."
									: name}
							</BoltSemiBoldText>
						</View>
					</View>
					<View style={tw`flex flex-row items-center`}>
						<IonIcons name="ios-star" size={13} color="#000" />
						<BoltSemiBoldText style={tw`text-lg ml-1`}>
							5
						</BoltSemiBoldText>
					</View>
				</View>
				<View style={tw`flex flex-row items-center`}>
					<BoltLightText style={tw`text-sm`}>
						{price} €
					</BoltLightText>
				</View>
			</View>
		</View>
	);
}
