import React from "react";
import { View, FlatList } from "react-native";
import tw from "tailwind-react-native-classnames";
import IonIcons from "react-native-vector-icons/Ionicons";
import { BoltSemiBoldText } from "../CustomText";
import { restaurants } from "../../constants";
import RestaurantCard from "./RestaurantCard";
import { ScrollView } from "react-native-gesture-handler";
import axios from "axios";
import { URL_BACK_END } from '../../Environment/Environment'

export default class RestaurantList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			idCategory: 0,
			nameCategory: "",
			foods: []
		}
	}

	componentDidMount() {
		this.loadData();
	}

	loadData = () => {
		const { idCategory, nameCategory } = this.props.route.params;
		axios.get(`${URL_BACK_END}/api/foods/category/${idCategory}`)
			.then(resp => {
				this.setState({
					foods: resp.data,
					idCategory: idCategory,
					nameCategory: nameCategory
				})
			})
	}

	render() {
		const { foods, nameCategory } = this.state;
		return (
			<ScrollView style={tw`flex mt-4 mx-5`}>
				<View style={tw`flex flex-row items-center justify-between`}>
					<BoltSemiBoldText style={tw`text-xl`}>
						{nameCategory}
					</BoltSemiBoldText>
				</View>

				<View>
					{foods.map((item) => (
						<RestaurantCard
							key={item.id}
							data={item}
							navigation={this.props.navigation}
						/>
					))}
				</View>
			</ScrollView>
		)
	}
}