import React from "react";
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    FlatList,
    TouchableOpacity,
    Button
} from "react-native";
import { StatusBar } from "expo-status-bar";
import tw from "tailwind-react-native-classnames";
import { BoltLightText, BoltSemiBoldText } from "../components/CustomText";
import Constants from "expo-constants";
import Ionicons from "react-native-vector-icons/Ionicons";
import axios from "axios";
import { URL_BACK_END } from "../Environment/Environment";

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            password: ""
        }
    }

    handleChange = (name, value) => {
        this.setState({
            [name]: value
        })
    };

    handleSubmit = (event) => {
        const {firstname, lastname, email, password} = this.state;

        let member = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            rank_id: 4
        }
        console.log(member)
        axios.post(`${URL_BACK_END}/api/members`, member, { withCredentials: true })
        .then(resp => {
            this.setState({ firstname: "", lastname: "", email: "", password: "" });
        })
    }

    render() {
        return (
            <View
                style={{
                    ...tw`flex pt-2 h-full pb-40`,
                    marginTop: Constants.statusBarHeight,
                }}
            >
                <StatusBar style="auto" />
                <BoltSemiBoldText style={tw`mx-5 mt-8 text-center pb-3 text-2xl`}>Inscription</BoltSemiBoldText>

                <View style={tw`mx-5 mt-8 pb-3`}>
                    <View style={tw`bg-gray-200 p-4 rounded-md flex flex-row mt-3`}>
                        <Ionicons
                            name="finger-print-outline"
                            size={18}
                            color="black"
                            style={tw`my-auto`}
                        />
                        <TextInput
                            style={tw`ml-3`}
                            name="firstname"
                            placeholder="Prénom"
                            onChangeText={(text) => this.setState({firstname: text})}
                            value={this.state.firstname}
                        />
                    </View>
                    <View style={tw`bg-gray-200 p-4 rounded-md flex flex-row mt-3`}>
                        <Ionicons
                            name="body-outline"
                            size={18}
                            color="black"
                            style={tw`my-auto`}
                        />
                        <TextInput
                            style={tw`ml-3`}
                            placeholder="Nom"
                            onChangeText={(text) => this.setState({lastname: text})}
                            value={this.state.lastname}
                        />
                    </View>
                    <View style={tw`bg-gray-200 p-4 rounded-md flex flex-row mt-3`}>
                        <Ionicons
                            name="at-circle-outline"
                            size={18}
                            color="black"
                            style={tw`my-auto`}
                        />
                        <TextInput
                            style={tw`ml-3`}
                            placeholder="Adresse e-mail"
                            onChangeText={(text) => this.setState({email: text})}
                            value={this.state.email}
                        />
                    </View>
                    <View style={tw`bg-gray-200 p-4 rounded-md flex flex-row mt-3`}>
                        <Ionicons
                            name="lock-closed-outline"
                            size={18}
                            color="black"
                            style={tw`my-auto`}
                        />
                        <TextInput
                            style={tw`ml-3`}
                            secureTextEntry={true}
                            placeholder="Mot de passe"
                            onChangeText={(text) => this.setState({password: text})}
                            value={this.state.password}
                        />
                    </View>
                    <View style={tw`mt-4`}>
                        <Button onPress={this.handleSubmit} title="S'inscrire" />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      marginHorizontal: 16,
    },
    title: {
      textAlign: 'center',
      marginVertical: 8,
    },
    fixToText: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    separator: {
      marginVertical: 8,
      borderBottomColor: '#737373',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
  });
  