import { StatusBar } from "expo-status-bar";
import React, { Fragment } from "react";
import {
	StyleSheet,
	Text,
	View,
	TextInput,
	FlatList,
	TouchableOpacity,
} from "react-native";
import tw from "tailwind-react-native-classnames";
import IonIcons from "react-native-vector-icons/Ionicons";
import { BoltLightText, BoltSemiBoldText } from "../components/CustomText";
import { popularFoods } from "../constants";
import { ScrollView } from "react-native-gesture-handler";
import Constants from "expo-constants";
import axios from "axios";
import { URL_BACK_END } from '../Environment/Environment'

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			categories: [],
			loaded: false
		}
	}

	componentDidMount() {
		this.loadData();
	}

	loadData = () => {
		axios.get(`${URL_BACK_END}/api/categories`)
			.then(resp => {
				this.setState({
					categories: resp.data.data,
					loaded: true
				})
			})
	}

	render() {
		const { categories, loaded } = this.state;

		return (
			<ScrollView
				style={{
					...tw`flex pt-2`,
					marginTop: Constants.statusBarHeight,
				}}
			>
				{loaded &&
					<Fragment>
						<StatusBar style="auto" />
						<View style={tw`flex flex-row items-center mx-5`}>
							<IonIcons name="ios-location-outline" size={25} color="black" />
							<BoltLightText style={tw`text-lg ml-2`}>
								Adresse user
							</BoltLightText>
						</View>

						<View style={tw`mx-5 mt-8 pb-3`}>
							<BoltSemiBoldText style={tw`text-sm`}>
								Nos catégories
							</BoltSemiBoldText>
						</View>

						<View style={tw`pb-20`}>
							<FlatList
								style={tw`mx-5`}
								data={categories}
								renderItem={({ item }) => (
									<TouchableOpacity key={item.id} 
									onPress={() =>
										this.props.navigation.navigate("Restaurant", {
											screen: "RestaurantList",
											params: {
												idCategory: item.id,
												nameCategory: item.attributes.name
											}
										})
									}>
										<BoltLightText
											style={tw`text-sm my-4 text-gray-700`}
										>
											{item.attributes.name}
										</BoltLightText>
									</TouchableOpacity>
								)}
								keyExtractor={(item) => item.id}
								showsVerticalScrollIndicator={false}
							/>
						</View>
					</Fragment>
				}

			</ScrollView>
		)
	}
}

export default Home;
